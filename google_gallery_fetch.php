<!-- this script gets all image urls in a google+ gallery to display in a gallery on our awards sites -->

<?php
   $images = array();

    // get images from feed
    if ($mode == 'gallery' || $mode == 'gallery_pictures') {
      $user_id = "107759455861829218670";

      $url = "http://photos.googleapis.com/data/feed/api/user/$user_id/albumid/$gallery[album_id]?alt=json";

      $ch = curl_init($url);

      $options = array(CURLOPT_RETURNTRANSFER => true);
      curl_setopt_array( $ch, $options );

      $feed = curl_exec($ch);

      $array_feed = json_decode($feed);

      foreach ($array_feed->feed->entry as $key => $feed_data) {
        $temp_array = array();

        $img_base_url = $feed_data->content->src;
        $img_title = $feed_data->title->{'$t'};
        $temp_array['large_image'] = str_replace("/$img_title", "/s900/$img_title", $img_base_url);
        $temp_array['s_thumb'] = str_replace('s72', 's72-c', $feed_data->{'media$group'}->{'media$thumbnail'}[0]->url);
        $temp_array['m_thumb'] = str_replace('s144', 's130-c', $feed_data->{'media$group'}->{'media$thumbnail'}[1]->url);
        $temp_array['l_thumb'] = str_replace('s288', 's288-c', $feed_data->{'media$group'}->{'media$thumbnail'}[2]->url);

        $images[] = $temp_array;
      }
    }
?>