// question was:
// Given a digit string, return all possible letter combinations that are valid words that the number could represent. 
// A mapping of digit to letters (just like on the telephone buttons) is given below.
// Assume you have access to an api to identify valid words

keypad_mapings =    [
                            [],
                            ['a','b','c'],
                            ['d','e','f'],
                            ['g','h','i'],
                            ['j','k','l'],
                            ['m','n','o'],
                            ['p','q','r','s'],
                            ['t','u','v'],
                            ['w','x','y','z']
                    ];
var keys = [1, 2, 3];
var final_result = get_all_words(keys);
console.log(get_all_words(keys));
document.write('The output from this code is:<br>When the keys: ' + keys + ' are pressed the result is: <br><br>' + final_result);

keypad_mapings =    [
                            [],
                            ['a','b','c'],
                            ['d','e','f'],
                            ['g','h','i'],
                            ['j','k','l'],
                            ['m','n','o'],
                            ['p','q','r','s'],
                            ['t','u','v'],
                            ['w','x','y','z']
                    ];
function get_all_words(pressed_buttons) {
    var letters = [];
    for(i=0; i<pressed_buttons.length; i++) {
        letters.push(keypad_mapings[pressed_buttons[i]]);
    }

    var progress = 0;
    current_word = '';
    limit = pressed_buttons.length;
    found_words = [];
    
    var return_value = recursive_word_generator(letters, progress, current_word, limit, found_words);

    return return_value;
}

function recursive_word_generator(letters, progress, current_word, limit, found_words) {
    if(progress == limit){
        if(api_call(current_word)) {
            found_words.push(current_word);
        }
    }else{
        //itterating button presses
        for(var i=0; i<letters[progress].length; i++) {
            var next_word = current_word + letters[progress][i];
            recursive_word_generator(letters, progress + 1, next_word, limit, found_words);
        }
    }
    return found_words
}

function api_call(word) {
    // assuming always true in absence of actual api;
    return true;  
}