<!-- 
question was:
Write a PHP class which will be used as a pagination helper.  The helper should accept 3 arguments, 
1) an array of items, 2) the number of items to be displayed on the page and 3) the page number you are currently on.  
The class should have methods to get the:

- next & previous page numbers
- the total count of items
- the items for the requested page 

-->


<?php
  $items = array('bob','tony','adam','bill', 'jane', 'mal', 'river');
  $items_per_page = 3;
  $current_page_no = 2;

  $pagination = new pagination_helper($items, $items_per_page, $current_page_no);

  class pagination_helper {
      
      var $items = false;
      var $items_per_page = false;
      var $current_page_no = false;

      // class constructor
      function __construct($items = false, $items_per_page = false, $current_page_no = false) {
        if($items != false && $items_per_page != false && $current_page_no != false){
          $this->items = $items;
          $this->items_per_page = $items_per_page;
          $this->current_page_no = $current_page_no;
        }else{
          die("missing argument error");
        }
      }

      // return total count of items in array
      function get_total_item_count() {
        return count($this->items);
      }

      // gets next and previous page numbers, false values represent null values (start and end of list)
      function get_related_page_numbers() {
        $first_page = 1;
        $items = $this->items;
        $items_per_page = $this->items_per_page;
        $return_values = array('previous' => false, 'next' => false);
        $current_page = $this->current_page_no;
        $last_page = ceil(count($items) / $items_per_page);

        // check page is in range
        if($current_page > $last_page){
          die('error: page out of range');
        }

        switch ($this->current_page_no) {
          case $first_page:
            $return_values['next'] = 2;
            break;
          case $last_page:
            $return_values['previous'] = $current_page - 1;
            break;
          default:
            $return_values['previous'] = $current_page - 1;
            $return_values['next'] = $current_page + 1;
        }
        return $return_values;
      }

      // returns items for current page
      function get_current_page_items() {
        $current_page = $this->current_page_no;
        $items = $this->items;
        $items_per_page = $this->items_per_page;
        $offset = ($current_page * ($items_per_page - 1)) - 1;

        $items_for_page = array_slice($items, $offset, $items_per_page);

        return $items_for_page;
      }
  }



  echo "getting related page numbers:<br>";
  print_r($pagination->get_related_page_numbers(3));
  echo "<br><br><br>";
  echo "getting total item count:<br>";
  print_r($pagination->get_total_item_count());
  echo "<br><br><br>";
  echo "getting items for page:<br>";
  print_r($pagination->get_current_page_items());