<?php

    // I wrote this while I worked at e partner digital, if I were doing it now I would have used mysqli and a switch statement instaid of multiple elseif statements

    //allow script to ecxeed default max execution time and memory limit
    ini_set("memory_limit", "512M");
    ini_set('max_execution_time', 9000);

    //set variables
    $OAuth_key = '#################################';
    $secret_key = '################################';
    $request_url = 'http://api.tumblr.com/v2/blog/sinstarclothing.tumblr.com/info?api_key='.$OAuth_key;

    //db credentials
    $db_name = "dev_test";
    $db_host = "localhost";
    $db_username = "root";
    $db_password = "";

    // make api call to determine number of posts
    $getresult = file_get_contents($request_url);

    //if request sucessfull continue
    if($getresult === FALSE) {
        echo "request failed";
    }else{
        echo "<p>Request sent</p>";

        $result = json_decode($getresult);
        
        $number_of_posts = $result->{'response'}->{'blog'}->{'posts'};

        //api can only return 50 posts at a time so must split task over multiple requests
        //this array stores the number of posts to be read by each request
        $num_requests = array();
        $round_num_requests = floor($number_of_posts / 50);

        $i=0;
        While($i < $round_num_requests){
            $num_requests[] = 50;
            $i++;
        }

        $remainder = floor($number_of_posts - (50 * $round_num_requests));
        if ($remainder > 0) {
            $num_requests[] = $remainder;
        }


        //set feed url
        $feedURL = "http://sinstarclothing.tumblr.com/api/read/?num=";
        
        echo "<p>number of posts: $number_of_posts</p>";

        // get posts from Tumblr

        //cache local files
        $i = 0;
        $counter = 1;

        //loop through each iteration of the feed and cache it in a file
        foreach ($num_requests as $value) {
           if(!file_exists("tumblr_".$counter.".xml")){
                $this_feedURL = $feedURL."50&start=".$i;
                echo "Generated file: tumblr_".$counter.".xml <br>";
                $getresult = file_get_contents($this_feedURL);
                file_put_contents("tumblr_".$counter.".xml", $getresult);
                flush();
            }
            $i = $i + 50;
            $counter++;
        }
        
        //loop through each cahced xml file and add them to the database
        $counter = 1;
        $posts_exported = 0;

        $xml = new DOMDocument; 

        //open database connection
        if (!$link = mysql_connect($db_host, $db_username, $db_password)) {
            echo 'Could not connect to database';
            exit;
        }
        if (!mysql_select_db($db_name, $link)) {
            echo 'Could not select database';
            exit;
        }
        //clear table before loop
        $clear_table = mysql_query("TRUNCATE TABLE posts", $link);

        foreach($num_requests as $value) {
            $xml -> load("tumblr_".$counter.".xml");

            $posts = $xml -> getElementsByTagName("post");

            $i = 0;
            while($i < $value){
                $post = $posts -> item($i);

                //store values in variables
                $tumblr_id = 0;
                $tumblr_id = mysql_real_escape_string(trim($post -> getAttribute('id')));

                //post text

                if(is_object($post -> getElementsByTagName('regular-body') -> item(0))){
                    $post_content = mysql_real_escape_string(trim($post -> getElementsByTagName('regular-body') -> item(0) -> textContent));
                }elseif(is_object($post -> getElementsByTagName('video-caption') -> item(0))){
                    $post_content = mysql_real_escape_string(trim($post -> getElementsByTagName('video-caption') -> item(0) -> textContent));
                }elseif(is_object($post -> getElementsByTagName('photo-caption') -> item(0))){
                    $post_content = mysql_real_escape_string(trim($post -> getElementsByTagName('photo-caption') -> item(0) -> textContent));
                }elseif(is_object($post -> getElementsByTagName('link-url') -> item(0))){
                    $post_content = mysql_real_escape_string(trim($post -> getElementsByTagName('link-url') -> item(0) -> textContent));
                }elseif(is_object($post -> getElementsByTagName('audio-caption') -> item(0))){
                    $post_content = mysql_real_escape_string(trim($post -> getElementsByTagName('audio-caption') -> item(0) -> textContent));
                }else{
                    $post_content = 'no content found';
                }
                //imge url
                $featured_image = "0";
                if(is_object($post -> getElementsByTagName('photo-url') -> item(0))){
                    $featured_image = mysql_real_escape_string(trim($post -> getElementsByTagName('photo-url') -> item(0) -> textContent));
                }else{
                    $featured_image = "no image found";
                }
                //audio url
                $featured_audio = "0";
                if(is_object($post -> getElementsByTagName('audio-player') -> item(0))){
                    $featured_audio = mysql_real_escape_string(trim($post -> getElementsByTagName('audio-player') -> item(0) -> textContent));
                }else{
                    $featured_audio = "no audio found";
                }
                //video url
                $featured_video = "0";
                if(is_object($post -> getElementsByTagName('video-source') -> item(0))){
                    $featured_video = mysql_real_escape_string(trim($post -> getElementsByTagName('video-source') -> item(0) -> textContent));
                }else{
                    $featured_video = "no video found";
                }
                $date = date('Y-m-d H:i:s', strtotime(mysql_real_escape_string(trim($post -> getAttribute('date')))));
                $slug = mysql_real_escape_string(trim($post -> getAttribute('slug')));
                $tags = "";
                $tags_list = $post -> getElementsByTagName('tag');
                $tag_counter = $tags_list->length;

                //loop throug all tags adding them to a comma deliniated list
                for ($for_counter = 0; $for_counter < $tag_counter; $for_counter++) {
                    $tags.= mysql_real_escape_string(trim($tags_list->item($for_counter)->nodeValue.","));
                }

                //remove trailing comma
                $tags = substr_replace($tags ,"",-1);

                $posts_exported++;
                $i++;

                //store in database
                $sql = "INSERT INTO posts (tumblr_id, post_content, featured_video, featured_audio, featured_image, tags, slug, post_date) VALUES (".$tumblr_id.", '".$post_content."', '".$featured_video."', '".$featured_audio."', '".$featured_image."', '".$tags."', '".$slug."', '".$date."')";
                
                $result = mysql_query($sql, $link);

                echo "<p>$posts_exported posts exported</p>";
                flush();
            }
            $counter++;
        }
    }
    echo $posts_exported . " posts were loaded into database";
?>